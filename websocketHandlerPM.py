import asyncio
import websockets
import json
import os
from datetime import datetime, timedelta
import socket
import time
import os

def load_env_file(filepath):
    with open(filepath, 'r') as file:
        for line in file:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                os.environ[key] = value

# Load the .env file
load_env_file('webTest1/.env')
ws_host = os.getenv('WS_HOST')

lock_file_path = 'websocket_server.lock'

# Lista de clientes conectados
clientes = set()

def create_lock_file():
    with open(lock_file_path, 'w') as f:
        f.write('running')

def delete_lock_file():
    os.remove(lock_file_path)

async def parse_data(data):
    items = data.split(',')
    parsed_data = [float(item) for item in items if item.strip() and '-' != item.strip() and len(item.strip()) == 6]
    return parsed_data

async def enviar_datos_a_clientes(datos):
    if clientes:
        datos_json = json.dumps(datos)
        await asyncio.gather(*(cliente.send(datos_json) for cliente in clientes))

async def recibir_datos(websocket, path):
    global clientes
    clientes.add(websocket)
    sample_rate = 4000  # Expected sample rate

    try:
        print("connecting to the power meter thread")
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(('127.0.0.1', 8765))
        
        accumulated_data = []
        end_time = time.time() + 1
        print("Start sending 4000 sps")

        while True:
            data = client.recv(1024).decode('utf-8')
            parsed_data = await parse_data(data)
            
            if parsed_data:
                accumulated_data.extend(parsed_data)
                
            if time.time() >= end_time:
                #print(accumulated_data)
                #print(len(accumulated_data))
                #break
                # Calculate the current timestamp
                current_time = datetime.now()
                time_delta = timedelta(seconds=1 / sample_rate)
                
                # Generate timestamps for the accumulated samples
                timestamps = [(current_time + i * time_delta).isoformat() for i in range(len(accumulated_data))]

                # Prepare data to be sent to clients
                datos = {"CH1": accumulated_data, "Tiempo": timestamps}
                await enviar_datos_a_clientes(datos)
                
                # Reset for the next second
                accumulated_data = []
                end_time = time.time() + 1

    except websockets.exceptions.ConnectionClosedError:
        pass
    finally:
        client.close()
        clientes.remove(websocket)

start_server = websockets.serve(recibir_datos, ws_host, 5050)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
