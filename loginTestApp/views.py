from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .CustomUserCreationForm import MyUserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages

def signup(request):
    if request.method == 'GET':
        return render(request, 'signup.html', {
            'form': MyUserCreationForm()
        })
    else:
        if request.POST['password1'] == request.POST['password2']:
            if request.POST['password1'] == request.POST['username']:
                return render(request, 'signup.html', {
                    'form': MyUserCreationForm(),
                    'error': "same username and password"
                })
            else:
                try:
                    user = User.objects.create_user(
                        username=request.POST['username'], password=request.POST['password1'])
                    user.save()
                    login(request, user)
                    return redirect('index')
                except:
                    return render(request, 'signup.html', {
                        'form': MyUserCreationForm(),
                        'error': 'user already exist'
                    })
        else:
            return render(request, 'signup.html', {
                'form': MyUserCreationForm(),
                'error': "Passwords doen't mach"
            })

    return render(request, 'signup.html', {
        'form': MyUserCreationForm()
    })
# --------------------------------------------------------------------------------------------------


def signout(request):
    logout(request)
    return redirect('index')
# --------------------------------------------------------------------------------------------------


def signin(request):
    if request.method == 'GET':
        return render(request, 'signin.html', {
        'form': AuthenticationForm
        })
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'signin.html',{
            'form': AuthenticationForm,
            'error': 'User name or password incorrect!'
            })
        else:
            login(request, user)
            messages.success(request, 'Login successful')
            return redirect('index')
    return render(request, 'signin.html', {
        'form': AuthenticationForm
        })   
#--------------------------------------------------------------------------------------------------