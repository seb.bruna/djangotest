import asyncio
import websockets
import json

async def listen():
    uri = "ws://localhost:5050"
    async with websockets.connect(uri) as websocket:
        while True:
            try:
                message = await websocket.recv()
                data = json.loads(message)
                print(f"Received data: {data}")
            except websockets.ConnectionClosed:
                print("Connection closed")
                break

asyncio.get_event_loop().run_until_complete(listen())
