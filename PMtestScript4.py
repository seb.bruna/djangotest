import socket
import time
import asyncio
async def parse_data(data):
    global count
    # Split the data on commas
    items = data.split(',')
    # Convert to floats, ignoring any invalid entries
    parsed_data = [float(item) for item in items if item.strip() and '-' != item.strip()]
    
    return len(parsed_data)
    
    return parsed_numbers

async def listen_to_data():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(('127.0.0.1', 8765))
    end_time = time.time() + 10  # Set the end time to be 1 second from now
    
    streamedData = []
    sps = 0
    try:
        while time.time() < end_time:  # Continue until 1 second has passed
            data = client.recv(1024).decode('utf-8')
            task = asyncio.create_task(parse_data(data))
            #if task:
            #    print(task)
            #else:
            #    break
            sps = sps + await task
            
    finally:
        print(sps)
        client.close()


asyncio.run(listen_to_data())
