from django.shortcuts import render, redirect
from .models import nodes, temperature, nodesRegister
from . import dbclass1 as dp
from .forms import createNewNode
# Create your views here.
from django.http import HttpResponse, JsonResponse
import subprocess
import psutil
#from . import websocketHandle
import os

#---------------------------------------------------------------------------
def load_env_file(filepath):
    with open(filepath, 'r') as file:
        for line in file:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                os.environ[key] = value

# Load the .env file
load_env_file('webTest1/.env')
db_host = os.getenv('DB_HOST')
ws_host = os.getenv('WS_HOST')
#---------------------------------------------------------------------------

def index(request):
    title = ''
   
    return render(request, 'index.html', {
        'title': title
    })

def about(request):
    title = 'about ioT'
    return render(request, 'about.html', {
        'title': title
    })

def sensors(request, id):
    
    return render(request, 'nodes.html')

def temp(request, id):
    raspi = dp.dbConnection(db_host, '5050', 'meteo', 'postgres', 'Admin.123')
    raspi.conn2dB()
    sqlQuery = 'SELECT ns.id, ns.nodenumber_id, ns.sampletime, tempe.temp, tempe."alarmTemp" FROM template1_temperature tempe INNER JOIN template1_nodes ns ON tempe.id = ns.id WHERE ns.nodenumber_id = ' + str(id)+' ORDER BY ns.id ASC;'
    tempData = raspi.query2dB(sqlQuery)

    #times = [record[2].strftime('%Y-%m-%d %H:%M:%S') for record in tempData]
    #data = [float(record[3]) for record in tempData]
    #context = {
    #    'times': times,
    #    'temperatures': data,
    #}
    
    return render(request, 'temperature.html',{
        'temp': tempData
    })
def test(request, id):
    raspi = dp.dbConnection(db_host, '5050', 'meteo', 'postgres', 'Admin.123')
    raspi.conn2dB()
    #sqlQuery = 'SELECT ns.id, ns.nodenumber_id, ns.sampletime, tempe.temp, tempe."alarmTemp" FROM template1_temperature tempe INNER JOIN template1_nodes ns ON tempe.id = ns.id WHERE ns.nodenumber_id = ' + str(id)+' ORDER BY ns.id ASC;'
    sqlQuery = "SELECT * FROM get_temperature_data(1, 2, 3, 4);"
    tempData = raspi.query2dB(sqlQuery)
    #print(tempData)
    times_1 = [record[0].strftime('%Y-%m-%d %H:%M:%S') for record in tempData]
    data_1 = [float(record[1]) for record in tempData]
    times_2 = times_1
    data_2 = [float(record[3]) for record in tempData]
    times_3 = times_1
    data_3 = [float(record[5]) for record in tempData]
    times_4 = times_1
    data_4 = [float(record[7]) for record in tempData]
    
    #print(data_4)
    return render(request, 'test.html',{
        'times_1': times_1,
        'data_1': data_1,
        'times_2': times_2,
        'data_2': data_2,
        'times_3': times_3,
        'data_3': data_3,
        'times_4': times_4,
        'data_4': data_4,
    })  

def createNode(request):
    
    if request.method == 'GET':
        return render(request, 'createNode.html',{
            'formulario': createNewNode()
        })
    else:
        nodeTime = request.POST['createdtime']
        redid = request.POST['red']
        sqlQuery = "INSERT INTO template1_nodesregister(createdtime, red_id) VALUES("+"'" +nodeTime+"'" +", "+ redid +")"
        raspi = dp.dbConnection(db_host, '5050', 'meteo', 'postgres', 'Admin.123')
        raspi.conn2dB()
        raspi.write2dB(sqlQuery)
        return redirect('index')


def rtplot(request):
    #lock_file_path = 'websocket_server.lock'
    #if not os.path.exists(lock_file_path):
    try:
        # Start the WebSocket server if it's not already running
        subprocess.Popen(['python', 'websocketHandlerPM.py'])
    except Exception as e:
        print(f"Error starting WebSocket server: {e}")
    return render(request, 'rtplot.html',{
        'callWebsocket': True,
        'ws_host': ws_host
    })
   
def check_if_process_running(process_name):
    # Check if there is any running process that contains the given name processName.
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if process_name.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False
    
