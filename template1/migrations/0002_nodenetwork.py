# Generated by Django 5.0.4 on 2024-05-05 15:24

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('template1', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='nodeNetwork',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updateTome', models.DateField()),
                ('minTemp', models.IntegerField()),
                ('maxTemp', models.IntegerField()),
                ('minHum', models.IntegerField()),
                ('maxHum', models.IntegerField()),
                ('red', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='template1.red')),
            ],
        ),
    ]
