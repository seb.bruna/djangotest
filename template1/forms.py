from django import forms

class createNewNode(forms.Form):
    red = forms.IntegerField(label='Id Red')
    createdtime = forms.DateField(
        label='Created Time',
        widget=forms.DateInput(attrs={'type': 'date'})  # Use the HTML5 date input type
    )
    location = forms.CharField(label='node location', max_length=200)
#class networkParameters(forms.From):
#    updateTome = forms.DateField()
#    minTemp = forms.IntegerField()
#    maxTemp = forms.IntegerField()
#    minHum = forms.IntegerField()
#    maxHum = forms.IntegerField()