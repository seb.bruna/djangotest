import psycopg2 as pg
import pandas as pd

class dbConnection(object):

    def __init__(self, host, port, dbName, user, password):
        self.host = host
        self.port = port
        self.dbName = dbName
        self.user = user
        self.password = password
    def conn2dB(self):
        self.connection = pg.connect(host=self.host, port=self.port, dbname=self.dbName, user=self.user, password=self.password)
        return "connection succesfull"
    def query2dB(self, incQuery):
        conn = self.connection
        cur = conn.cursor()
        cur.execute(incQuery)
        return cur.fetchall()
            
    def write2dB(self,incQuery):
        conn = self.connection
        try:
            cur = conn.cursor()
            cur.execute(incQuery)
            conn.commit()
            return "write successful"
        except Exception as error:
            conn.commit()
            print("An error occurred:", error)
    def dfQuery(self, incQuery):
        return pd.read_sql_query(sqlQuery,self.connection)


