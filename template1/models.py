from django.db import models

class red(models.Model):
    userid = models.IntegerField()
    location = models.CharField(max_length=300)

class nodesRegister(models.Model):
    red = models.ForeignKey(red, on_delete=models.CASCADE)
    createdtime = models.DateField()
   
class nodes(models.Model):
    
    #id Django automaticamente crea el campo ID como SERIAL no nulo
    #  id = models.AutoField(primary_key=True), AutoField sirve por
    # si queremos que la llave primaria sea otra.
    sampletime = models.DateTimeField()
    nodenumber = models.ForeignKey(nodesRegister, on_delete=models.CASCADE)
    def __str__(self):
        return "Node number: " + str(self.nodenumber) + "   SampleTime: " + self.sampletime.strftime("%Y-%m-%d %H:%M%S")

class temperature(models.Model):
    alarmTemp = models.BooleanField()
    temp = models.DecimalField(max_digits=4, decimal_places=2)
    nodes = models.ForeignKey(nodes, on_delete=models.CASCADE)
    def __str__(self):
        return "Node number: " + str(self.nodes.nodenumber) + " temp: " + str(self.temp * 100)


class humidity(models.Model):
    alarmHum = models.BooleanField()
    humidity = models.DecimalField(max_digits=4, decimal_places=2)
    nodes = models.ForeignKey(nodes, on_delete=models.CASCADE)

class nodeNetwork(models.Model):
    red = models.ForeignKey(red, on_delete=models.CASCADE)
    updateTome = models.DateField()
    minTemp = models.IntegerField()
    maxTemp = models.IntegerField()
    minHum = models.IntegerField()
    maxHum = models.IntegerField()

    
