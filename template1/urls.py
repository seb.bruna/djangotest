from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    
    path('about/', views.about, name='about'),

    path('sensors/', views.sensors, {'id': 1}, name='sensors'),
    path('sensors/<int:id>/', views.sensors),
    
    path('temperature/', views.temp, {'id': 1}, name='temperature'),  # Default ID set to 1
    path('temperature/<int:id>/', views.temp),
    
    path('test/', views.test, {'id': 1}, name='test'),  # Default ID set to 1
    path('test/<int:id>/', views.test),

    path('createNode/', views.createNode, name='createNode'),
    path('rtplot/', views.rtplot, name='rtplot'),
]