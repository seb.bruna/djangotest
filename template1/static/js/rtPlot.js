document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('realtimeChart').getContext('2d');
    var realtimeChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [], // Timestamps
            datasets: [{
                label: 'Channel 1',
                borderColor: 'rgb(255, 99, 132)',
                data: []
            }]
        },
        options: {
            animation: false,
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'second',
                        tooltipFormat: 'HH:mm:ss.SSS',
                        displayFormats: {
                            second: 'HH:mm:ss.SSS'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Time'
                    }
                },
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    //var ws_host = "{{ ws_host }}";
    var websocket = new WebSocket('ws://' + ws_host + ':5050/ws');
    //var websocket = new WebSocket('ws://localhost:5050/ws');
    websocket.onmessage = function (event) {
        var data = JSON.parse(event.data);
        var times = data.Tiempo;
        var ch1 = data.CH1;

        if (times && ch1 && times.length === ch1.length) {
            for (let i = 0; i < ch1.length; i++) {
                if (realtimeChart.data.labels.length > 500) { // Limit data points for performance
                    realtimeChart.data.labels.shift();
                    realtimeChart.data.datasets[0].data.shift();
                }

                // Add each data point to the chart
                realtimeChart.data.labels.push(times[i]);
                realtimeChart.data.datasets[0].data.push(ch1[i]);
            }
            realtimeChart.update(); // Update the chart after adding all new data points
        }
    };

    websocket.onerror = function (event) {
        console.error('WebSocket error:', event);
    };

    websocket.onclose = function (event) {
        console.log('WebSocket connection closed:', event);
    };
});