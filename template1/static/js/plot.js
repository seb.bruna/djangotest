document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('myChart1').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: times_1,
            datasets: [{
                label: 'Temperature over Time',
                data: data_1,
                borderColor: 'rgba(255, 0, 0, 1)',
                borderWidth: 1.5
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                x: {
                    type: 'time',
                    time: {
                        tooltipFormat: 'yyyy-MM-dd HH:mm:ss',
                    },
                    title: {
                        display: true,
                        text: 'Date',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Temperature [°C]',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.raw.y;
                        }
                    }
                }
            }
        }
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('myChart2').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: times_2,
            datasets: [{
                label: 'Temperature over Time',
                data: data_2,
                borderColor: 'rgba(100, 200, 100, 1)',
                borderWidth: 1.5
            }]
        },
        options: {
            scales: {
                x: {
                    type: 'time',
                    time: {
                        tooltipFormat: 'yyyy-MM-dd HH:mm:ss',
                    },
                    title: {
                        display: true,
                        text: 'Date',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Temperature [°C]',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.raw.y;
                        }
                    }
                }
            }
        }
    });
});


document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('myChart3').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: times_3,
            datasets: [{
                label: 'Temperature over Time',
                data: data_3,
                borderColor: 'rgba(0, 0, 255, 1)',
                borderWidth: 1.5
            }]
        },
        options: {
            scales: {
                x: {
                    type: 'time',
                    time: {
                        tooltipFormat: 'yyyy-MM-dd HH:mm:ss',
                    },
                    title: {
                        display: true,
                        text: 'Date',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Temperature [°C]',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.raw.y;
                        }
                    }
                }
            }
        }
    });
});

document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('myChart4').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: times_4,
            datasets: [{
                label: 'Temperature over Time',
                data: data_4,
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1.5
            }]
        },
        options: {
            scales: {
                x: {
                    type: 'time',
                    time: {
                        tooltipFormat: 'yyyy-MM-dd HH:mm:ss',
                    },
                    title: {
                        display: true,
                        text: 'Date',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                },
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Temperature [°C]',
                        font: {
                            weight: 'bold'  // Make x-axis title bold
                        },
                        color: '#000000'  // Change y-axis title color
                    }
                }
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return tooltipItem.raw.y;
                        }
                    }
                }
            }
        }
    });
});
